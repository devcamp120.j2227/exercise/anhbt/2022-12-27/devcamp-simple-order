import Order from "./components/Order";
import ProductDetail from "./components/ProductDetail";

const routerList = [
    { label: "Order", path: "/",  element: <Order/>},
    { label: "Order", path: "/*",  element: <Order/>},
    { label: "Product", path: "/product/:id", element: <ProductDetail/>}
]

export default routerList