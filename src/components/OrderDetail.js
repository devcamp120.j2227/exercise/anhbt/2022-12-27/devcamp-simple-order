import React, { useState } from 'react'
import { Button, Card, CardBody, CardText, CardTitle } from 'reactstrap'
import { useNavigate } from 'react-router-dom'

function OrderDetail({buy ,...props}) {
    const navigate = useNavigate();
    const [quantity, setQuantity] = useState(0);
    const buyHandler = () => {
        console.log(props, quantity);
        setQuantity(quantity+1);
        buy(props.price);
    }
    const detailHandler = ()=> {
        navigate(`/product/${props.id}`)
    }
    return (
        <Card 
            body
            style={{
                width: '33%'
            }}>
            <CardBody>
                <CardTitle tag="h5">{props.name}</CardTitle>
                <CardText>Price: ${props.price}</CardText>
                <CardText>Quantity: {quantity}</CardText>
                <Button color='primary' onClick={buyHandler}> + Buy </Button>
                <Button color='info' onClick={detailHandler}> Detail </Button>
            </CardBody>
        </Card>
    )
}

export default OrderDetail