import React from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { Button, Card, CardBody, CardText, CardTitle } from 'reactstrap'
import { products } from '../App'

function ProductDetail() {
    const {id} = useParams();
    const navigate = useNavigate()
    const backHandler = () => {
        navigate("/");
    }
    return (
        <Card 
            body
            style={{
                width: '33%'
            }}>
            <CardBody>
                {
                products[id] ?
                    <>
                        <CardTitle tag="h5">{products[id].name}</CardTitle>
                        <CardText>Price: ${products[id].price}</CardText>
                    </> :
                        <CardTitle tag="h5">No Product</CardTitle> 
                }
                <Button color='info' onClick={backHandler}> Back </Button>
            </CardBody>
        </Card>
    )
}

export default ProductDetail