import React, { useState } from 'react'
import { Container, Label } from 'reactstrap'
import { products } from '../App';
import OrderDetail from './OrderDetail'



function Order() {
    const [total, setTotal] = useState(0);
    const onBuy = (price) => {
        setTotal(
            total + price
        )
    }
    return (
        <Container>
            <Label tag={"h5"}>Order</Label>
            <div style={{display: "flex"}}>
                {products.map((productDetail,index)=>{
                    return <OrderDetail key={index} {...productDetail} buy={onBuy}/>
                })}
            </div>
            <Label tag={"h5"}>Total : ${total}</Label>
        </Container>
    )
}

export default Order