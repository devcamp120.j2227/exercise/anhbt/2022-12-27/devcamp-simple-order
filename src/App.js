import { Route, Routes } from "react-router-dom";
import routerList from "./routes";

export const products = [
    {
        id: 0,
        name: 'Iphone X',
        price: 900
    },
    {
        id: 1,
        name: 'Samsung S9',
        price: 800
    },
    {
        id: 2,
        name: 'Nokia 8 ',
        price: 600
    },
]

function App() {
	return (
		<div>
			<Routes>
			{routerList.map((router, index) => {
				return (router.path) ? 
				<Route 
					key={index} 
					exact 
					path={router.path} 
					element={router.element}
				/> 
				:
				null
			})}
			</Routes>
		</div>
	);
}

export default App;
